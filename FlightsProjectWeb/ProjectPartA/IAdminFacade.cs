﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProjectPartA
{
    public interface IAdminFacade
    {
        void CreateAirline(LoginToken<Administrator> token, AirlineCompany airlineCompany);
    }
}

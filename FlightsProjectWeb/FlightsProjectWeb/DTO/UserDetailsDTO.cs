﻿using System.ComponentModel.DataAnnotations;

namespace FlightsProjectWeb.DTO
{
    public class UserDetailsDTO
    {
        [Required]
        [StringLength(maximumLength: 20, MinimumLength = 5)]
        public string Name { get; set; }

        [Required]
        [StringLength(maximumLength: 20, MinimumLength = 5)]
        public string Password { get; set; }
    }
}